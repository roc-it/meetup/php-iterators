'use strict';

// TODO : add more console.log to add details

/**
 * @param {ReadableStream<Uint8Array>} stream
 *
 * @return {AsyncGenerator<Object>}
 */
async function* convertStreamToJsonItems(stream) {
    const reader             = stream.getReader();
    const decoder            = new TextDecoder();
    let previousDecodedChunk = null;

    try {
        while (true) {
            const {done, chunk} = await reader.read();
            if (true === done) {
                console.log('Stream done');
                return;
            }
            let decodedChunk = decoder.decode(chunk);
            if (null !== previousDecodedChunk) {
                decodedChunk         = previousDecodedChunk + decodedChunk;
                previousDecodedChunk = null;
            }

            let items = decodedChunk.split('\n');
            for (let index in items) {
                let item        = items[index];
                let isJsonValid = true;
                let parsedItem  = null;

                try {
                    parsedItem = JSON.parse(item);
                } catch (e) {
                    isJsonValid = false;
                }

                if (true === isJsonValid) {
                    yield parsedItem;
                } else {
                    // TODO throw an error if not last item
                    previousDecodedChunk = item;
                }
            }
        }
    } finally {
        reader.releaseLock();
        console.log('Stream finished');
    }
}

const stream = await fetch(`/some-url`)
    .then((response) => response.body);

const items = await stream
    .then((stream) => convertStreamToJsonItems(stream));

(async () => {
    for await (let item of items) {
        // apply your own logic
        // or cancel after 100 items :
        // stream.cancel();
    }
})();
