* [Sommaire](./00-sommaire.md)
* [Suivant]()

# Préambule sur les `array`

On a tous déjà vu / produit du code comme ça :

```php
<?php

declare(strict_types=1);

namespace App;

final class SomeService
{
    /**
     * @param SomeOtherClass[] $objectList
     *
     * @return int[]
     */
    public function doSomeStuff(array $objectList): array
    {
        // some logic
    }
}
```

<details>
<summary>Mais que contient `$objectList` ?</summary>
A priori `SomeOtherClass[]` indique quelque chose dans ce goût la :

```php
$objectList = [
    new SomeOtherClass(),
    new SomeOtherClass(),
    new SomeOtherClass(),
    new SomeOtherClass(),
    // ...
];
```

Mais concrètement on n'en est pas sûr sans vérifier chaque item dans une boucle.
</details>

<details>
<summary>Est-on sûr qu'il s'agit d'une `list` (`array` indexé par des entiers consécutifs commençant par `0`) ?</summary>
En effet `array` n'indique rien de plus qu'un ensemble d'objets.
On peut avoir comme dans l'exemple précédent:

```php
$objectList = [
    new SomeOtherClass(),
    new SomeOtherClass(),
    new SomeOtherClass(),
    new SomeOtherClass(),
    // ...
];
```
 
ou bien

```php
$objectList = [
    1 => new SomeOtherClass(),
    0 => new SomeOtherClass(),
    2 => new SomeOtherClass(),
    3 => new SomeOtherClass(),
    // ...
];
```

Quelle différence me direz-vous ? Essayer le code ci-dessous :

```php
<?php

declare(strict_types=1);

$list = [
    'hello',
    'world',
];

$orderedList = [
    1 => 'hello',
    0 => 'world',
];

var_dump('Liste : ');
$jsonEncodedList = json_encode($list);
var_dump($jsonEncodedList);
var_dump(json_decode($jsonEncodedList));

var_dump('Liste ordonnée : ');
$jsonEncodedOrderedList = json_encode($orderedList);
var_dump($jsonEncodedOrderedList);
var_dump(json_decode($jsonEncodedOrderedList));
```

Voila comment on passe d'un tableau d'éléments à un objet sans le vouloir.
</details>

<details>
<summary>Est-ce un tableau associatif ?</summary>
A priori non car si c'était un tableau associatif on aurait :

```php
<?php

declare(strict_types=1);

namespace App;

final class SomeService
{
    /**
     * @param array<string, SomeOtherClass> $objectList
     *
     * @return int[]
     */
    public function doSomeStuff(array $objectList): array
    {
        // some logic
    }
}
```

De cette manière on spécifie le fait qu'il y a une clé et le type de la clé.
Ne pas confondre `SomeOtherClass[]` (liste au sens PHP) et `array<int, SomeOtherClass>` (les clés ne sont pas nécessairement croissantes ni contigües).
</details>

<details>
<summary>Je vais appliquer une logique sur cette liste ici, mais comment faire si cette logique doit aussi être appliquée ailleurs ?</summary>
</details>

<details>
<summary>Quelle différence (d'un point de vue typage) avec `$myList = ['hello', 'world']` ?</summary>
</details>

<hr />
Nous allons vérifier si les `Iterator` natifs de PHP peuvent nous aider à résoudre tout cela.

Nos objectifs seront donc d'avoir un code mieux typé, avec une logique rangée au bon endroit et une ré-utilisabilité.
