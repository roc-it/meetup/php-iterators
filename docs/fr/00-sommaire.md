Les opinions exprimées dans ce document n'engagent que moi.

# Introduction

# Sommaire

* [Préambule sur les `array`](./01-preambule.md)
* [Problème 1 : La gestion de la mémoire]()
* [Problème 2 : La duplication de logique]()
* [Problème 3 : Le typage]()
* [Exemples d'usage]()
  * [Les classes natives]()
    * [Filtre d'une collection Doctrine]()
    * [Map d'une collection d'objet vers une autre]()
  * [Export d'un fichier via stream]()
  * [Stream beaucoup de résultats JSON]()
